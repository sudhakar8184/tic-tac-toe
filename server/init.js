
module.exports =(app,http)=>{

require('dotenv').config({ silent: true });
require('./app/models/users')
require('./app/models/gamelist')
require('./app/models/room')
require('./app/models/message')
require('./config/socket')(app,http)
require('./config/db')
    require('./config/routes/index')(app)
}
