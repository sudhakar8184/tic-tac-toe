
const auth = require('../../../app/controllers/auth')
const user = require('../../../app/controllers/user')
const chat = require('../../../app/controllers/chat')
const is_auth = require('./../../middleware/is_auth')
const router = require('express').Router();
router.post('/signup',auth.signUp)
router.post('/login',auth.logIn)
router.get('/getDetails',[is_auth,user.getDetails])
router.post('/updatePoints',[is_auth,user.updatePoints])
router.post('/updateGameData',[is_auth,user.updateGameData])
router.get('/getGameData/:username',[is_auth,user.getGameData])
router.post('/updateMessage',[is_auth,chat.updateMessage])
router.get('/getMessagesConnected/:roomname',[is_auth,chat.getMessagesConnected])
router.get('/getConnectedUsers/:username',[is_auth,chat.getConnectedUsers])
router.get('/redisclear',[is_auth,user.redisclear])
module.exports = router