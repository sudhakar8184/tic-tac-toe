const sockets = require('./../app/services')
module.exports =  (app,server) => {
    const io = require('socket.io').listen(server);
    if (io) {
        io.set("origins", '*:*');
        io.on('connection', (socket) => {
            console.log("connected successfull",socket.id)
            io.sockets.connected[socket.id].emit('socket-id', socket.id);
           sockets(socket,io)
           
        });
        app.set('socketio', io);
    }
};