var jwt = require('jsonwebtoken');
 const secret =require('./../../../config').secret
module.exports ={
    createToken : (user)=>{
        let today = new Date();
        let exp = new Date(today);
        exp.setDate(today.getDate()+60);
    
        return jwt.sign({
            id: user._id,
            username:user.username,
            email: user.email,
           
            // exp: parseInt(exp.getTime() / 1000),
        }, secret);
    }
}