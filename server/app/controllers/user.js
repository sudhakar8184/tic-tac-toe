const Mongoose = require('mongoose')
const User = Mongoose.model('Users')
const Gamelist = Mongoose.model('Gamelist')
const socketService = require('./../services/socketservice')
module.exports= {
    getDetails : async(req,res) => {
        try{
            let user  =req.user
            user  = await User.findOne({username:user.username})
             return res.json({success: true,data:user})
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
    },
    updatePoints : async(req,res) => {
        try{
            let user  = req.user
            let data = req.body
                user  = await User.update({username:data.username},{$inc :{points:data.points}})
           
                return res.json({success: true,data:user})
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
    },
    updateGameData:async(req,res) => {
      try{
        let main = req.body
        let user = new Gamelist(main)
          user =   await user.save();
         return res.json({success: true,data:user})
      }
     catch(err){
       console.log("error",err)
     return res.json({success: false,data:{err}})
     }
    },
     redisclear : async(req,res)=>{
      socketService.cleardata()
     },

     getGameData:async(req,res) => {
      try{
        let username = req.params.username
        console.log(">>>>>>>>>>>>>>",username)
        let user = await Gamelist.find({username:username})
         return res.json({success: true,data:user})
      }
     catch(err){
       console.log("error",err)
     return res.json({success: false,data:{err}})
     }
    }
    
}