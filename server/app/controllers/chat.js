const Mongoose  = require('mongoose')
const Room  = Mongoose.model('Rooms')
const Message  = Mongoose.model('Messages')
module.exports= {
    updateMessage:async(req,res) => {
      try{
        let main = req.body
        let message = new Message(main)
        message = await message.save()
         return res.json({success: true,data: ''})
      }
     catch(err){
       console.log("error",err)
     return res.json({success: false,data:{err}})
     }
    },
     redisclear : async(req,res)=>{

     },

     getMessagesConnected:async(req,res) => {
      try{
        let roomname = req.params.roomname
        let user = await Message.find({roomName:roomname})
         return res.json({success: true,data:user})
      }
     catch(err){
       console.log("error",err)
     return res.json({success: false,data:{err}})
     }
    },
    getConnectedUsers:async(req,res) => {
        try{
          let username = req.params.username
          console.log(">>>>>>>>>>>>>>",username)
          let user = await Room.find({users:{$all:[username]}})
           return res.json({success: true,data:user})
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
      }
    
}