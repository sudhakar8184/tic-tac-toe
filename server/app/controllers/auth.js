const Mongoose = require('mongoose')
const User = Mongoose.model('Users')
const bcrypt = require('bcrypt')
const jwtService =require('./v1/jwt')
const redis =require('./../services/socketservice')
module.exports= {
    signUp : async(req,res) => {
        try {
            let main = req.body
            let user = new User(main)
             user.password = bcrypt.hashSync(user.password,bcrypt.genSaltSync(8))
              user =   await user.save();
             return res.json({success: true,data:user})
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
    },
    logIn : async(req,res) => {
        try{
            let main = req.body
            let user = await User.findOne({username: main.username})
             if(bcrypt.compareSync(main.password,user.password)){
                 user = {
                    username : user.username,
                    token: jwtService.createToken(user),
                    _id: user._id
                  }
                return res.json({success: true,data:user })
             } else {
                return res.json({success: false,data:"" })
             }
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
    },
    authication : async(req,res) => {
        try{
            let main = req.body
            let user = await User.findOne({username: main.username})
             if(user){
                
                return res.json({success: true,data:user })
             } else {
                return res.json({success: false,data:"" })
             }
        }
       catch(err){
         console.log("error",err)
       return res.json({success: false,data:{err}})
       }
    },
    cleardata : async(req,res) => {
      try{
         let user  = req.user
         if(user.username == 'sudhakar'){
            redis.cleardata()
         }
      }
     catch(err){
       console.log("error",err)
     return res.json({success: false,data:{err}})
     }
  }
}