const redis = require('./../../config/redis')

const rediscache = {
   setRedis: async(key, value) => {
    value  = JSON.stringify(value)

      await redis.set(key,value)
    },
     getRedis: async(key) => {
      let val =  await redis.get(key)
      return JSON.parse(val)
    }
}
module.exports = rediscache
