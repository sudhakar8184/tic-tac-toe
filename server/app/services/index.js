const socketService = require('./socketservice')
const Mongoose  = require('mongoose')
const Room  = Mongoose.model('Rooms')
module.exports = async (socket,io)=>{
           socket.on('set-data', async function(data){
               socketService.setuserdata(data,io)
             let messageread =  await socketService.getChatMessagesRead()
             io.sockets.connected[socket.id].emit('messageread',messageread)
            })

            socket.on('join',(room,username)=>{
               // socketService.cleardata()
              socket.join(room)
              socketService.setChatRoom(room,username)
              socketService.setChatMessagesRead(room)
            })

            socket.on('message-reset',(users)=>{
               socketService.setChatMessagesRead(users.room,users.username,0)
            })

            socket.on('typeing',(room)=>{
               socket.broadcast.to(room).emit('show_typeing',room)
             })

             socket.on('stopped',(room)=>{
               socket.broadcast.to(room).emit('show_stopped',room)
             })

            socket.on('iconnected', async (room)=>{
               let length =0;
               let chatroom=  await socketService.getChatRoom() 
               if(chatroom && chatroom[room]){
                  length = Object.keys(chatroom[room]).length
               }
               console.log("????room active",room,chatroom)
               if(length == 2){
                  io.in(room).emit('show_iconnected',{room:room,active:true})
               } else {
                  io.in(room).emit('show_iconnected',{room:room,active:false})
               }
             })
             

            socket.on('message',async(message)=>{
              let usernames =[]
               let chatroom=  await socketService.getChatRoom() 
               if(chatroom && chatroom[message.roomName]){
                  usernames = Object.keys(chatroom[message.roomName])
               }
               let index =0
               if(usernames.length) {
                  index =  usernames.indexOf(message.sender)
                 index = index == 0 ?1 :0
                  message.active= false;
               }
            
              socketService.setChatMessagesRead(message.roomName,usernames[index],1)
               socket.broadcast.to(message.roomName).emit('message',message)
             })

            socket.on('get-users-online', async function(data) {
                let userdata =  await socketService.getuserdata(data)
                console.log("datatat",userdata)
                if(userdata) {
                    io.emit('show-user-online-init',userdata)
                }
             })

             socket.on('update-users-connect',  function(data){
               socketService.updateuserdata(data,'',io)
               let userdata ={}
               Object.keys(data).map((user)=>{
                  userdata[user] = data[user].socket
               })
               Object.keys(data).map((user)=>{
                 if(data[user].to){
                    let socket =  userdata[data[user].to]
                    io.sockets.connected[socket].emit('message-popup',user);
                 }
               })
               io.emit('show-update-users-connect',data)
             })

             socket.on('connect-for-game', async function(data){
                     console.log("connect-for-game",data)
                     // await  io.sockets.connected[data.socket]
                     let room=[]
                     if(io.sockets.connected[data.socket]){
                        io.sockets.connected[data.socket].emit('show-connect-for-game',data.game);
                     }
                     if(data.starter && data.game.recieved)
                    room = await Room.find({users:{$all:[data.starter,data.game.recieved]}})
                   console.log(">?>?> rom",room)
                   let newroom ={
                     users:[data.starter,data.game.recieved],
                     roomName:data.starter+'_'+data.game.recieved
                  }
                     if(!room.length){
                        newroom= new Room(newroom)
                       newroom.save()
                     socket.join(room.roomName)
                     socketService.setChatMessagesRead(room)
                     socketService.setChatRoom(room.roomName,data.game.recieved)
                     io.sockets.connected[data.socket].emit('chat-connection',newroom)
                     }
                     
                    
             })

            socket.on('game-changes', async function(data){
                console.log(":::::::::::::::::::::::::::::::::::::::",data.socket)
               // await  io.sockets.connected[data.socket]
                if(io.sockets.connected[data.socket]){
                   io.sockets.connected[data.socket].emit('game-over-update',data.game);
                }
               })

               socket.on('end', async function(data){
                  let userdata =  await socketService.getuserdata('usersdata')
                  console.log("?>?>?>? end preview",userdata)
                  const preview =  await socketService.getuserdata('usersdata')
                  let socket;
                  let user
                 
                  Object.keys(userdata).map((ele)=> {
                     if(userdata[ele].to === data.username){
                       socket = userdata[ele].socket
                       userdata[ele].to = ''
                       userdata[ele].connect = false
                     }
                  })
                  console.log('end end unser',data)
                  delete userdata[data.username.toString()]
                  await socketService.updateuserdata(userdata,data.username,io)
                  console.log('end end',socket)
                  if(preview[data.username].connect){
                     io.sockets.connected[socket].emit('show-user-online-disconnect',{userdata:userdata,preview : preview, type:'end'})
                  }else{
                     io.emit('show-user-online-init',userdata)
                  }
                 
                  // io.sockets.connected[data.socket].emit('resetdata',{userdata:userdata,game:data.game})
                 })

               socket.on('disconnect', async() => {
                  console.log("::::::??????????????????????",socket.id)
                  if(socket.id){
                     let userdata =  await socketService.getuserdata('usersdata')
                     const  preview = await socketService.getuserdata('usersdata')
                     const  socketId = await socketService.getsocketdata('usersdata')
                     console.log("::::::??????????????????????",socketId)
                     let alldata
                     let userto;
                     let user
                     Object.keys(userdata).map((ele)=>{
                        if(userdata[ele].socket === socket.id){
                          user = ele
                        }
                     })
                     console.log(user)
                     Object.keys(userdata).map((ele)=>{
                        if(userdata[ele].to === user){
                          userdata[ele].to = ''
                          userdata[ele].connect = false
                          socket = userdata[ele].socket
                        }
                     })
                     delete userdata[user]
                     io.emit('show-room-disconnected',socketId[socket.id]);
                     await socketService.updatechatroom(socketId[socket.id])
                     await socketService.updateuserdata(userdata,user,io)
                     io.emit('show-user-online-disconnect',{userdata:userdata,preview : preview,type:'disconnect'})
                  }
              });
           
}