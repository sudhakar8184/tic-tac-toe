const rediscache = require('./rediscache')
module.exports = {
     setuserdata: async(data,io) => {
        let userdata  =  await rediscache.getRedis('usersdata')
        let socketId  =  await rediscache.getRedis('socketid')
        if(!userdata){
            userdata={}
        }
        if(userdata.null){
           delete userdata['null']
        }
        userdata[data.username] = {
            socket: data['socket'],
            to:'',
            connect: false,
            username:data.username,
            iscompleted : false
        }
        Object.keys(userdata).map((user)=>{
            if(!io.sockets.connected[userdata[user].socket] || !io.sockets.connected[userdata[user].socket].connected){
              delete userdata[user]
            }
        })
        socketId = socketId ? socketId:{}
        socketId[data['socket']] = data.username
        console.log("{{{{{{{{{{{{{{{{{{{{{{{{{",userdata,data)
       await rediscache.setRedis('usersdata',userdata)    
       await rediscache.setRedis('socketid',socketId)   
    },
    getuserdata: async(data) => {
        let userdata  =  await rediscache.getRedis(data)
        return userdata
    },
    updateuserdata : async(data,username,io) => {
        let userdata  =  await rediscache.getRedis('usersdata')
        Object.keys(userdata).map((user)=>{
            if(data[user]){
                userdata[user] = data[user]
            }
        })
        delete userdata[username]
        await rediscache.setRedis('usersdata',userdata)   
    },
    setChatRoom : async(room,username) => {
        let chatroom  =  await rediscache.getRedis('chatroom')
        if(chatroom && chatroom[room]){
        chatroom[room][username]=1
        } else {
            if(!chatroom)
              chatroom={}
         chatroom[room]={}
         chatroom[room][username] =1
        }
        await rediscache.setRedis('chatroom',chatroom)   
    },
    cleardata : async() =>{
        await rediscache.setRedis('usersdata','')   
        await rediscache.setRedis('chatroom','')
        await rediscache.setRedis('chatmessagesread','') 
    },
    getChatRoom: async() =>{
       return await rediscache.getRedis('chatroom')
    },
    updatechatroom :async(user)=>{
        let chatroom  =  await rediscache.getRedis('chatroom')
        if(chatroom && Object.keys(chatroom).length){
            Object.keys(chatroom).map((room)=>{
             delete chatroom[room][user]
            })
        }
       
        await rediscache.setRedis('chatroom',chatroom)  
    },
    getsocketdata : async()=>{
        return await rediscache.getRedis('socketid')
    },
    setsocketdata : async(id)=>{
     let socketId =    await rediscache.getRedis('socketid')
     delete socketId[id]
     await rediscache.setRedis('socketid',socketId)  
    },
    setChatMessagesRead : async(room,username=undefined,value=undefined) => {
        let chatmessagesread  =  await rediscache.getRedis('chatmessagesread')
        console.log(">>>>>>>>> chatmessagesread",chatmessagesread,room,username)
        if(chatmessagesread && chatmessagesread[room]){
        if(username && value){
            chatmessagesread[room][username] = chatmessagesread[room][username]+value
            await rediscache.setRedis('chatmessagesread',chatmessagesread)   
         } else if(username&&value === 0){
            chatmessagesread[room][username] = value
            await rediscache.setRedis('chatmessagesread',chatmessagesread)   
         }
        } else if(room){
        if(!chatmessagesread)
            chatmessagesread={}
        chatmessagesread[room]={}
        let datausername = room.split('_')
            if(!chatmessagesread[room][datausername[0]]) chatmessagesread[room][datausername[0]] = 0
            if(!chatmessagesread[room][datausername[1]]) chatmessagesread[room][datausername[1]] = 0
            
            await rediscache.setRedis('chatmessagesread',chatmessagesread)   
        }
       },
       getChatMessagesRead: async() =>{
       let data =  await rediscache.getRedis('chatmessagesread')
       console.log(">LLLLLLLLchatmessagesread",data)
       return data ? data : {};
         },
}