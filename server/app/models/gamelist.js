const mongoose  = require('mongoose')
let Schema = mongoose.Schema
var gameListSchema = new Schema({
    username:{
        type:Array,
        trim:true,
        default:[],
    },
    gameWinner:{
        type:String,
        trim:true,
        default:''
    },
    tossWinner:{
        type:String,
        trim:true,
        default:''
    },
    pattern:{
        type:Array,
        trim:true,
        default:[],
    },
    date:{
         type : Date,
          default: Date.now
    }
})

module.exports = mongoose.model('Gamelist',gameListSchema)
