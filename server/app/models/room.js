const mongoose = require('mongoose')

const Schema = mongoose.Schema

const roomSchema = new Schema({
    roomName :{
        type:String,
        trim:true,
        default:'',
        unique:true
    },
    users:{
        type:Array,
        trim:true,
        default:[],
    },
    not_read : {
        type:Object,
        trim:true,
        default:{},
    }
})
module.exports = mongoose.model('Rooms',roomSchema)

