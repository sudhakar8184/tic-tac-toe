const mongoose  = require('mongoose')
let Schema = mongoose.Schema
var userSchema = new Schema({
    username:{
        type:String,
        trim:true,
        default:'',
        unique:true
    },
    email:{
        type:String,
        trim:true,
        default:''
    },
    points:{
        type:Number,
        trim:true,
        default:100
    },
    location:{
        type:String,
        trim:true,
        default:''
    },
    password:{
        type:String,
        trim:true,
        default:''
    },
    createdAt:{
        type:Date,
        trim:true,
        default: Date.now()
    }

    
})

module.exports = mongoose.model('Users',userSchema)
