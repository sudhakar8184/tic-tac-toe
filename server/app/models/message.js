const mongoose = require('mongoose')

const Schema = mongoose.Schema

const messageSchema = new Schema({
    roomName :{
        type:String,
        trim:true,
        default:'',
    },
    sender:{
        type:String,
        trim:true,
        default:'',
    },
    message:{
        type:String,
        trim:true,
        default:'',
    },
    active:{
        type:Boolean,
        default:true, 
    },
    deleted:{
        type:Boolean,
        default:false,
    },
    createdAt: {
        type:Date,
        trim:true,
        default: Date.now()
    }
})
module.exports = mongoose.model('Messages',messageSchema)

