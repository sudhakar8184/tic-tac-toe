var express = require("express");  
var cors = require("cors");  
var app = express();
const path = require('path')
const bodyParser =require('body-parser')
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
const port  = process.env.PORT || 7000;
var http = require('http').createServer(app);
require("./server/init")(app,http)
const isDev = process.env.NODE_ENV == 'production';
if(isDev){
        app.use(express.static(path.resolve(__dirname, 'build')))
    }
app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "./build/index.html"));
});


http.listen(port, function() {  
    console.log("My API is running...",port);
});