import React from 'react';
import './App.css';
import Home from './components/home';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Dasboard from './components/dasboard/dasboard';
import Gamelist from './components/gamelist';

function App() {
  return (
    <div className="App">
     <Router>
     <Route exact path="/" component={Home} />
     <Route exact path="/:username/gamelist" component={Gamelist} />
     <Route exact path="/:username" component={Dasboard} />
    
     </Router>
    
    </div>
  );
}

export default App;
