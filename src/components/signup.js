import React , { Component}  from 'react';
import {connect} from'react-redux';
import {signUp,empty} from './store/actions/authaction'

class Signup extends Component {

    constructor(props){
       super(props);
       this.state = {
           username : '',
           email : '',
           password:'',
           location:'',
           error :{
            username : '',
            email : '',
            password:'',
            location:'',
           }
       }
      this.handleChanges =  this.handleChanges.bind(this)
      this.submitChanges =  this.submitChanges.bind(this)
    }
    componentWillMount(){
      this.props.empty()
    }
    
    handleChanges = (event)=>{
     let name = event.target.name
     let value = event.target.value
     this.setState({
     [name] : event.target.value
     })
     let error = { ...this.state.error}
     error =  this.validation(name,value,error)
     this.setState({error :error})
    }

    submitChanges = (event)=>{
      event.preventDefault();
      let valid = true;
      let error = {...this.state.error}
      Object.keys(error).map((value)=>{
       error = this.validation(value.toString(),this.state[value],error)
        if(error[value] !== ''){
         valid = false
        }
      })
      
      this.setState({error})
     if(valid){
      let data = { ...this.state}
      delete data.error
       this.props.signUp(data).then((res)=>{
         if(res.data.success){
           this.props.handleLoginAndSignup()
         }
       })
     }
     }

  validation(name,value,error) {
    switch(name){
      case 'username':
          error.username = value  ? value.length > 3 ? "" : 'please enter data length more than 3': 'please enter data'
          break;
      case 'email':
          error.email = value ? /[a-z0-9]+@[a-z]+\.[a-z]+/gmi.test(value)? '' : 'please enter valid email': 'please enter data'
          break;
      case 'location':
          error.location = value  ? '': 'please enter data'
          break;
      case 'password':
           error.password = value ? value.length > 3 ? "" : 'please enter password length more than 3': 'please enter data'
           break;
      default:
           break;
    }
    return error
  }
  
    render(){
      let errorStyle={
        color:'red'
      }
        return(
            <div className="signup">
              <h2>SignUp Here</h2>
                <form onSubmit={this.submitChanges}>
                    <div className="form-group">
                    <label>UserName</label>
                      <input type="text" name="username" onChange ={this.handleChanges} className ="form-control"  value={this.state.username}/>
                    </div>
                    {this.state.error.username && <p style={errorStyle}>{this.state.error.username}</p>}
                    {!this.props.status && <p style={errorStyle}> This username is already used</p>}
                    <div className="form-group">
                     <label>Email</label>
                      <input type="email" name="email" onChange ={this.handleChanges} className ="form-control"  value={this.state.email}/>
                      {this.state.error.email && <p style={errorStyle}>{this.state.error.email}</p>}
                    </div>
                    <div className="form-group">
                     <label>Location</label>
                      <input type="text" name="location" onChange ={this.handleChanges} className ="form-control"  value={this.state.location}/>
                      {this.state.error.location && <p style={errorStyle}>{this.state.error.location}</p>}
                    </div>
                    <div className="form-group">
                     <label>Password</label>
                      <input type="password" name="password" onChange ={this.handleChanges} className ="form-control"  value={this.state.password}/>
                      {this.state.error.password && <p style={errorStyle}>{this.state.error.password}</p>}
                    </div>
                    <div className="form-group">
                      <button type="submit" className ="form-control" > submit </button>
                    </div>
                    </form>
            </div>
        );
    }
}

const mapStateToProps = (state)=>{
 
  return  {
    status : state.auth.status,
    data:  state.auth.data
  }
}



export default connect(mapStateToProps,{ signUp ,empty})(Signup)
