import React , { Component}  from 'react';
import Signup from './signup';
import Login from './login';

class Home extends Component {

    constructor(props){
       super(props);
       this.state = {
           login:false,
           signup:true
       }
    //    this.handleLoginAndSignup =this.handleLoginAndSignup.bind(this)
    }
    
    handleLoginAndSignup(data){
      this.setState({
        login: data === 'login'? true:false,
        signup: data === 'signup'? true:false
      })
    }
    render(){
        return(
            <div>
             <header>
            <nav>
                <a className="logo" href="javascript:void(0)"><img src="http://sudhakarreddy.herokuapp.com/images/logo.png" width="40px" height="40px"/></a>
                <a href="javascript:void(0)">Home</a>
                <div className="navright">
                <a href="javascript:void(0)" onClick={()=>{this.handleLoginAndSignup('login')}}>login</a>
                <a href="javascript:void(0)" onClick={()=>{this.handleLoginAndSignup('signup')}}>Signup</a>
                </div>
            </nav>
              </header>
              <section className="mainclass">
                   <article>
                       <div className="register">
                       {this.state.signup && <Signup handleLoginAndSignup ={()=>this.handleLoginAndSignup('login')}/> }
                       {this.state.login && <Login   /> }
                       {this.state.login && <p> here you <a href="#" onClick={()=>{this.handleLoginAndSignup('signup')}}>Register</a></p> }
                       {this.state.signup && <p> here you <a href="#" onClick={()=>{this.handleLoginAndSignup('login')}}>Login</a></p> }
                       </div>
                   </article>
                   <article>
                       
                   </article>
              </section>
              
            </div>
        );
    }
}

export default Home;
