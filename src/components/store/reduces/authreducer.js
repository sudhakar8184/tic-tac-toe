import { SIGNUP, LOGIN, GETDETAILS} from './../actions/type';
const initState = {
    data: '',
    status:true,
    redirect:false
};
const authReducer = (state =initState , action)=>{
    let newState = {...state}
    switch(action.type){
     case SIGNUP:
      if(action.data.success) {
        newState.status =  true
        newState.data =  action.data
       } else {
        newState.status =  false
        newState.data =  ""
      }
     break;
     case LOGIN:
      if(action.data.success) {
        newState.status =  true
        newState.data =  action.data
        newState.redirect = true
       } else {
        newState.status =  false
        newState.data =  ""
      }
     break;
     case GETDETAILS:
      if(action.data.success) {
        newState.status =  true
        newState.data =  action.data
       } else {
        newState.status =  false
        newState.data =  ""
        newState.redirect = true
      }
     break;
     case 'EMPTY':
     newState = initState
     break;
     default:
      break;
    }
    
return newState
}
export default authReducer;