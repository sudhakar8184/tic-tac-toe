import {UPDATEMESSAGES,GETCONNECTEDUSERS,GETMEESSAGESCONNECTED} from './../actions/type'
import { socket } from './../actions/socket';
import { join } from 'path';
const initState = {
    updateMessage: {
        state:true
    },
    getConnectedUsers:[],
    getMessagesConnected:[]
};
const chatReducer = (state =initState , action)=>{
    let newState = {...state}
    switch(action.type){
     case UPDATEMESSAGES:
      if(action.data.success) {
        newState.updateMessage.status = true
       } else {
        newState.updateMessage.status = false
      }
     break;
     case GETCONNECTEDUSERS:
      if(action.data.success) {
        action.data.data.map((room)=>{
          socket.emit('join',room.roomName,localStorage.getItem('username'))
          console.log("room.roomNameroom.roomNameroom.roomName",socket)
          socket.emit('iconnected',room.roomName)
          room['active'] = false
          room['not_read'] = 0
        })
        newState.getConnectedUsers =  action.data.data
       } else {
        newState.getConnectedUsers = []
      }
     break;
     case GETMEESSAGESCONNECTED : 
      if(action.data.success) {
      newState.getMessagesConnected =  action.data.data
     } else {

    }
   break;
     case 'EMPTY':
     newState = initState
     break;
     default:
      break;
    }
    
return newState
}
export default chatReducer;