import {combineReducers} from 'redux';
import authReducer from './authreducer';
import usersReducer from './usersreducer';
import gameReducer from './gamereducer';
import chatReducer from './chatreducer'

const rootReducer = combineReducers({
    auth:authReducer,
    user:usersReducer,
    game: gameReducer,
    chat:chatReducer
})

export default rootReducer;