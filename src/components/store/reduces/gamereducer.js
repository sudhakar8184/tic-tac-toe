import { GAMEMULTI,GAMEINIT,GETGAMEDATA} from './../actions/type'
const initState = {
    single: {
        player1: 'X',
        player2: 'O',
        system: '',
        user: '',
        coinValue: "Head",
        tossChoose: "Head",
        rotate: false,
        tossWin: '',
        playingCoinType:'',
        count:{
            system:0,
            user:0
        },
        positions:{
            1:'',
            2:'',
            3:'',
            4:'',
            5:'',
            6:'',
            7:'',
            8:'',
            9:''
        },
        winPositions:{
            1:[1,2,3],
            2:[4,5,6],
            3:[7,8,9],
            4:[1,4,7],
            5:[2,5,8],
            6:[3,6,9],
            7:[1,5,9],
            8:[3,5,7],
        },
        error:{
         message:''
        },
        win:'',
        username: localStorage.getItem('username')
    },
    multi:{
        username: localStorage.getItem('username'),
        alldata: {},
        socket: {
            other: '',
            socket: '',
            to: '',
            connect: false
        },
        game: {
            player1: 'X',
            player2: 'O',
            user1: '',
            user2: '',
            coinValue: "Head",
            tossChoose: {
                user1: 'Head',
                user2: 'Tail'
            },
            rotate: false,
            tossWin: '',
            playingCoinType: '',
            count: {
                user1: 0,
                user2: 0
            },
            positions: {
                1: '',
                2: '',
                3: '',
                4: '',
                5: '',
                6: '',
                7: '',
                8: '',
                9: ''
            },
            winPositions: {
                1: [1, 2, 3],
                2: [4, 5, 6],
                3: [7, 8, 9],
                4: [1, 4, 7],
                5: [2, 5, 8],
                6: [3, 6, 9],
                7: [1, 5, 9],
                8: [3, 5, 7],
            },
            error: {
                message: ''
            },
            win: '',
            username1: '',
            username2: '',
            socket: [],
            starter: '',
            recieved: '',
            connect:false,
        }
    },
    gameList:[]
    
};
const gameReducer = (state = initState , action)=>{
    let newState = {...state}
    switch(action.type){
     case GAMEMULTI:
        newState.multi =  true
     break;
     case GAMEINIT:
     initState.single.positions = {1:'',2:'',3:'',4:'',5:'',6:'',7:'',8:'',9:''}
     initState.multi.game.positions = {1:'',2:'',3:'',4:'',5:'',6:'',7:'',8:'',9:''}
     newState = initState
     break;
     case GETGAMEDATA : 
     if(action.data && action.data.success) 
     newState.gameList =  action.data.data
      break;
     default:
      break;
    }
    
return newState
}
export default gameReducer;