import environment from './../../environment/environment'
import * as io from 'socket.io-client';
const socket = io.connect(environment.socket);

 socket.on('socket-id',function(data){
     window.localStorage['socket']= data
 })
 
 export { socket}
