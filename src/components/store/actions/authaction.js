
import { SIGNUP, LOGIN,GETDETAILS} from './type'
import environment from './../../environment/environment'
export function signUp(data){
    return (dispatch)=>{
        return fetch(environment.url+'signup',{
            method:'post',
            mode: 'cors',
            body: JSON.stringify(data),
            headers:{
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*'
            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:SIGNUP,data:res})
        })
    }
  
  }

  export function logIn(data){
    return (dispatch)=>{
        return fetch(environment.url+'login',{
            method:'post',
            mode: 'cors',
            body: JSON.stringify(data),
            headers:{
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*'
            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
            if(res.success){
                window.localStorage['username']=res.data.username
                window.localStorage['token']=res.data.token
                window.localStorage['id']=res.data._id
               }
           return dispatch({type:LOGIN,data:res})
        })
    }
  }

  export function getDetails(){
    return (dispatch)=>{
        return fetch(environment.url+'getDetails',{
            method:'get',
            mode: 'cors',
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*',

            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:GETDETAILS,data:res})
        })
    }
  }
  export function empty(){
    return (dispatch)=>{
           return dispatch({type:"EMPTY",data:""})
    }
  }