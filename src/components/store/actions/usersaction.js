import { UPDATEPOINTS,UPDATEGAMEDATA,GETGAMEDATA,UPDATEMESSAGES,GETCONNECTEDUSERS,GETMEESSAGESCONNECTED} from './type'
// import redis from './redis'
// import socket from './socket'
import environment from './../../environment/environment'

export function updatePoints(data){
    return (dispatch)=>{
        return fetch(environment.url + 'updatePoints', {
            method:'post',
            mode: 'cors',
            body: JSON.stringify(data),
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*'
            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:UPDATEPOINTS,data:res})
        })
    }
  
  }

  export function updateGameData(data){
    return (dispatch)=>{
        return fetch(environment.url + 'updateGameData', {
            method:'post',
            mode: 'cors',
            body: JSON.stringify(data),
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*'
            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:UPDATEGAMEDATA,data:res})
        })
    }
  
  }

  export function getGameData(){
    return (dispatch)=>{
        return fetch(environment.url+'getGameData/'+localStorage.getItem('username'),{
            method:'get',
            mode: 'cors',
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*',

            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:GETGAMEDATA,data:res})
        })
    }
  }

  export function getMessagesConnected(roomname){
    return (dispatch)=>{
        return fetch(environment.url+'getMessagesConnected/'+roomname,{
            method:'get',
            mode: 'cors',
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*',

            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:GETMEESSAGESCONNECTED,data:res})
        })
    }
  }
  export function updateMessage(message){
    return (dispatch)=>{
        return fetch(environment.url+'updateMessage/',{
            method:'POST',
            mode: 'cors',
            body:JSON.stringify(message),
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*',

            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:UPDATEMESSAGES,data:res})
        })
    }
  }
  export function getConnectedUsers(){
    return (dispatch)=>{
        return fetch(environment.url+'getConnectedUsers/'+localStorage.getItem('username'),{
            method:'get',
            mode: 'cors',
            headers:{
                'username': localStorage.getItem('username'),
                'Authorization': localStorage.getItem('token'),
                "Content-Type" : "application/json",
                'Access-Control-Allow-Origin':'*',

            }
        }).then((res)=>{
                return res.json()
        }).then((res)=>{
           return dispatch({type:GETCONNECTEDUSERS,data:res})
        })
    }
  }