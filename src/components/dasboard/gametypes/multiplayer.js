import React, { Component } from 'react'
import { connect } from 'react-redux';
import { getDetails, empty } from '../../store/actions/authaction'
import { GameMulti,GameInit} from '../../store/actions/gameaction'
import { updatePoints,updateGameData } from '../../store/actions/usersaction'
import { BrowserRouter as Switch, Route, Redirect } from "react-router-dom";
import { socket } from './../../store/actions/socket';
class Multiplayer extends Component {
     maindata = {}
    constructor(props) {
        super()
        this.state = {...props.gameType,meassagePopup: false}
        this.handleCoinToss = this.handleCoinToss.bind(this)
        this.maindata = Object.assign({},props.gameType)
        this.playAgain = this.playAgain.bind(this)

        socket.on('game-over-update', (data) => {
            this.setState({
                game : data
            })

            if(data.win && data.win !== 'draw'){
                this.updatePointScore()
                this.props.multiplayerOn(false)
            }

            if(data.win){
                setTimeout(() => {
                    this.playAgain()
                }, 3000);
            }
        })
        
        socket.on('show-user-online-disconnect', (main) => {
            let socket = this.state.socket
            let game = this.state.game
            let state = this.state
            let  username = this.state.username
            let checkcondition
            // if(main.type && main.type == 'end'){
                checkcondition = main.preview
            //  }
            //  if(main.type && main.type == 'disconnect'){
            //     checkcondition = main.userdata
            //  }
            if(socket.to){
              let userto =   Object.keys(main.userdata).filter((ele) => ele == socket.to)
             
              if(socket.connect && !game.win){
                let data = {
                    username:'',
                    points: 0
                }
                  if(!game.win && checkcondition[username] && checkcondition[username].connect && game.starter === this.state.username) {
                      data.username = this.state.username
                      data.points= 100
                      this.updatePoints(data)
                    //   this.props.UpdatePointsHandle(data)
                      data.username = game.recieved
                      data.points= -100
                      setTimeout(() => {
                        this.updatePoints(data)
                      }, 100);
                  }
                  if(!game.win && checkcondition[username] && checkcondition[username].connect && game.recieved === this.state.username){
                    data.username = this.state.username
                    data.points= 100
                    this.updatePoints(data)
                    // this.props.UpdatePointsHandle(data)
                    data.username = game.starter
                    data.points= -100
                    setTimeout(() => {
                        this.updatePoints(data)
                      }, 100);
                }
                this.gameDataUpadte()
              }
           if(!userto.length) {
                socket.to = ''
                socket.connect = false
                this.props.multiplayerOn(false)
                game.connect= false
             }
            }

            this.setState({
                socket: socket,
                alldata: main.userdata,
                game: game
             })
             this.props.GameInit()
        })

        socket.on('show-user-online-init', (data) => {
            let socket = this.state.socket
            let game = this.state.game
            if(socket.to){
              let userto =   Object.keys(data).filter((ele) => ele == socket.to)
           if(!userto.length) {
                socket.to = ''
                socket.connect = false
                // this.resetdata()
                game = this.maindata.game
             }
            }
            this.setState({
                socket: socket,
                alldata: data,
                game:game
              })
        })

        socket.on('show-update-users-connect', (data) => {
            let socket = this.state.socket
            console.log(data,this.state)
            Object.keys(data).map((ele) => {
                if (ele == this.state.socket.to && data[ele].to && data[ele].to != this.state.username) {
                    socket.to = ''
                    socket.other = ''
                }

                if(data[ele].to == this.state.username){
                    
                }
            })
            this.setState({
                socket: socket,
                alldata: data
            })
        })

        socket.on('show-connect-for-game', (data) => {
            if (data.recieved !== this.state.username && this.state.socket.to &&this.state.alldata[this.state.socket.to]) {
                //  this.props.GameMulti()
                this.props.multiplayerOn(true)
                this.setState({
                    socket: { ...this.state.socket, other: this.state.alldata[this.state.socket.to].socket, connect: true },
                    game: { ...this.state.game,roomName:data.roomName,recieved :data.recieved, starter: this.state.username, username1: this.state.username, username2: this.state.socket.to, socket: [this.state.socket.socket, this.state.socket.other],connect:true }
                })
                this.updateChangesUser()
                this.props.updateChatRoom(this.state.game.roomName)
            } else {
                socket.emit('update-users-connect', this.state.alldata)
            }
        })
    }

    updateChangesUser(){
        setTimeout(() => {
            socket.emit('game-changes', { socket: this.state.socket.other, game: this.state.game })
        }, 5);
    }
     

    componentWillMount() {
        setTimeout(()=>{
            this.oninit()
        },1000)
       this.setState({
           username: localStorage.getItem('username')
       })
    }

    oninit(){
        this.props.multiplayerOn(false)
        socket.emit('set-data',{
            username:localStorage.getItem('username'),
            socket:localStorage.getItem('socket')
        })
        socket.emit('get-users-online', 'usersdata') 
    }

    componentWillUnmount(){
        this.setState({})
    }

 
   resetdata() {
    let alldata = this.state.alldata
    Object.keys(alldata).map((ele) => {
        if (ele == this.state.username) {
            alldata[ele].to = ''
            alldata[ele].connect = false
        }
        if (alldata[ele].to == this.state.username) {
            alldata[ele].to = ''
        }
    })
    console.log(this.maindata)
        Object.keys(this.maindata).map((ele)=>{
            this.setState({[ele]:this.maindata[ele]})
        })
    socket.emit('update-users-connect', alldata)
    }

    handleTossChoose(data) {
        let state = this.state
        if (state.game.starter === state.username) {
            state.game.tossChoose.user1 = data
            state.game.tossChoose.user2 = data === 'Head' ? 'Tail' : 'Head'
        } else {
            state.game.tossChoose.user2 = data
            state.game.tossChoose.user1 = data === 'Head' ? 'Tail' : 'Head'
        }
        this.setState({
            game:state.game 
        })
        this.updateChangesUser()
    }

    handleCoinToss(event) {
        var choose = Math.floor(Math.random() * (12 - 5)) + 5
        var b = 0
        this.setState({
            game: { ...this.state.game, rotate: true }
        })
        this.state.game = { ...this.state.game, rotate: true };
        
        

        var st = setInterval(() => {
            if (choose == b) {
                console.log('game',this.state.game)
                clearInterval(st)
                this.setState({
                    game: { ...this.state.game, rotate: false }
                })
                // this.updateChangesUser()
                setTimeout(() => {
                    if (this.state.game.tossChoose.user1 == this.state.game.coinValue) {
                        this.setState({
                            game: {
                                ...this.state.game,
                                tossWin:this.state.game.starter,
                                user1: 'player1',
                                user2: 'player2',

                                playingCoinType: 'player1'
                            }
                        })
                    } 
                    if (this.state.game.tossChoose.user2 == this.state.game.coinValue) {
                        this.setState({
                           game: {...this.state.game,
                            tossWin:this.state.game.recieved,
                            user1: 'player2',
                            user2: 'player1',
                            playingCoinType: 'player1'}

                        })
                        
                    }
                    this.updateChangesUser()
                },100)
            }
             
            this.setState({
                game: {
                    ...this.state.game,
                    coinValue: this.state.game.coinValue == 'Head' ? 'Tail' : 'Head'
                }
            })
            this.updateChangesUser()
            b = b + 1;

        }, 1000)

    }
    handlePosition(positionIndex) { 
        let state = this.state
        if (state.game.starter === state.username) {
            if(this.state.game.playingCoinType === this.state.game.user1){
                this.userHandler(positionIndex,this.state.game.user1)
                let playingCoinType = this.state.game.playingCoinType == 'player1' ? 'player2' : 'player1'
                this.setplayingCoinType(playingCoinType)
            }
        } 
         if(state.game.recieved === state.username){
            if(this.state.game.playingCoinType === this.state.game.user2){
                this.userHandler(positionIndex,this.state.game.user2)
                let playingCoinType = this.state.game.playingCoinType == 'player1' ? 'player2' : 'player1'
                this.setplayingCoinType(playingCoinType)
            }
        }
            this.updateChangesUser()
        }

    getHowManyFree() {
        let postion = { ...this.state.game.positions }
        return Object.keys(postion).filter(ele => postion[ele])
    }

    setplayingCoinType(playingCoinType) {
        this.setState({
            game: {
                ...this.state.game,
                playingCoinType: playingCoinType
            }
        })

    }

   updatePoints(data){
    this.props.updatePoints(data).then((res)=>{
        if(res.data && res.data.success){
            this.props.UpdatePointsHandle()
        }
    })
   }


    updatePointScore(){
        let data = {
            username:this.state.username,
            points: 0
        }
           
            if(this.state.game.starter === this.state.username && this.state.game.win == 'user1'){
                data.points = 100;
                this.updatePoints(data)
                // this.props.UpdatePointsHandle(data)
            }
            if(this.state.game.starter === this.state.username && this.state.game.win != 'user1'){
                data.points = -100;
                data.type = 'sub'
                this.updatePoints(data)
                // this.props.UpdatePointsHandle(data)
            }
            if(this.state.game.recieved === this.state.username && this.state.game.win == 'user2'){
                data.type = 'add'
                data.points = 100;
                this.updatePoints(data)
                // this.props.UpdatePointsHandle(data)
            }
            if(this.state.game.recieved === this.state.username && this.state.game.win != 'user2'){
                data.type = 'sub'
                data.points = -100;
                this.updatePoints(data)
                // this.props.UpdatePointsHandle(data)
            }
            // setTimeout(() => {
            //     this.resetdata()
            // }, 1000);
    }

    winPosition(user1, user2) {
        let win;
        (Object.keys(this.state.game.winPositions).map((ele) => {
            let user1count = [];
            let user2count = [];
            this.state.game.winPositions[ele].map((ele1) => {
                if (this.state.game.positions[ele1] == user1) {
                    user1count.push(ele1)
                }
                if (this.state.game.positions[ele1] == user2) {
                    user2count.push(ele1)
                }
            })
            if (user1count.length == 3) {
                win = 'user1'
            } else if (user2count.length == 3) {
                win = 'user2'
            } else if(!win){
                if(this.getHowManyFree().length == 9){
              win = 'draw'
                }
            }
        }))

        if (win) {
            this.setState({
                game: {
                    ...this.state.game,
                    win: win
                }
            })
               this.updatePointScore()
               this.props.multiplayerOn(false)
               this.gameDataUpadte()
               setTimeout(() => {
                   this.playAgain()
               }, 3000);
        }
    }

    userHandler(positionIndex,user) {
        let state = { ...this.state }
        let user1 = state.game[state.game.user1]
        let user2 = state.game[state.game.user2]
        if (!state.game.positions[positionIndex] && !this.state.game.win) {
            state.game.positions[positionIndex] = this.state.game[user]
            this.setState({
                game: {
                    ...this.state.game,
                    positions: state.game.positions
                }
            })
            setTimeout(() => {
                this.winPosition(user1, user2)
                // socket.emit('game',state.game)
            }, 5)

        } else {

            // this.setState({
            //     error.me : state.game.positions
            // })
        }
    }

     gameDataUpadte(){
       let gameData ={}
       gameData['username'] = [this.state.game.starter,this.state.game.recieved]
       gameData['gameWinner'] = this.state.username
       gameData['tossWinner'] = this.state.game.tossWin
       gameData['pattern'] = this.state.game.positions
       this.props.updateGameData(gameData).then((res)=>{
        if(res.data && res.data.success){
        }
    })
    }

    playAgain() {
       this.resetdata()
       this.oninit()
    }

    addToUser(data) {
        let alldata = this.state.alldata
        Object.keys(alldata).map((ele) => {
            if (alldata[ele].to == this.state.username) {
                alldata[ele].to = ''
            }
            if (ele == this.state.username) {
                alldata[ele].to = data
            }
        })
        this.setState({
            socket: { ...this.state.socket, to: data },
            alldata: alldata
        })
        socket.emit('update-users-connect', alldata)
    }
    

    connectToUser(data) {
        let alldata = this.state.alldata
        Object.keys(alldata).map((ele) => {
            if (ele == this.state.username) {
                alldata[ele].connect = true
                alldata[ele].to = data
            }

            if (ele == data) {
                alldata[ele].connect = true
            }
        })
        this.setState({
            socket: { ...this.state.socket, to: data, connect: true, other: alldata[data].socket, socket: alldata[this.state.username].socket },
            game: { ...this.state.game, recieved: this.state.username,connect:true,roomName:data+'_'+this.state.username},
            alldata: alldata,
        })
        setTimeout(() => {
            socket.emit('update-users-connect', alldata)
        setTimeout(() => {
            socket.emit('connect-for-game', { socket: alldata[data].socket, game: this.state.game,starter:data })
        }, 10);
    }, 10);
    this.props.multiplayerOn(true)
    this.props.updateChatRoom(data+'_'+this.state.username)
    }

    render() {
        let listdata = []
        Object.keys(this.state.alldata).map((ele,i) => {
            if (this.state.socket.to == '') {
                if (this.state.username !== ele && this.state.alldata[ele].to == ''&& !this.state.alldata[ele].connect) {
                    listdata.push(<li key={i}>{ele}<span>
                        <button onClick={() => { this.addToUser(ele) }}>add</button></span>
                    </li>)
                } else if (this.state.alldata[ele].to == this.state.username) {
                    listdata.push(<li  key={i}>{ele}<span>
                        <button onClick={() => { this.connectToUser(ele) }}>connect</button></span>
                    </li>)
                }
            } else {
                if (this.state.socket.to == ele) {
                    listdata.push(<li  key={i}>{ele}<span>
                        wait for response</span>
                    </li>)
                }
            }
        })

        let animationStyle = {
            animation: this.state.game.rotate && 'spin .8s infinite linear'
        }
        if (!this.state.socket.connect ) {
            return (
                <div>
                    <ul>
                        game changelle has 100 points
                        {listdata}
                    </ul>

                </div>
            )
        }
        return (
            <div>
                { this.state.socket.connect &&this.state.game.username1 && this.state.game.username2 &&
                <div>
                    { !this.state.game.user1 && 
                        <div className="single-header">
                        <p>please to choose to win</p>
                        {this.state.game.starter == this.state.username &&
                            <div>
                                <input type="radio" checked={this.state.game.tossChoose.user1 === 'Head'} name="toss" value="Head" onChange={() => this.handleTossChoose('Head')} />Head
                        <input type="radio" checked={this.state.game.tossChoose.user1 === 'Tail'} name="toss" value="Tail" onChange={() => this.handleTossChoose('Tail')} />Tail
                        </div>

                        }

                        {this.state.game.recieved == this.state.username &&
                            <div>
                                <input type="radio" checked={this.state.game.tossChoose.user2 === 'Head'} name="toss" value="Head" onChange={() => this.handleTossChoose('Head')} />Head
                        <input type="radio" checked={this.state.game.tossChoose.user2 === 'Tail'} name="toss" value="Tail" onChange={() => this.handleTossChoose('Tail')} />Tail
                        </div>
                        }

                        {this.state.game.starter === this.state.username && <button onClick={this.handleCoinToss}>TakeToss</button>}
                    </div>
                    }
                    

                    <div className="single-header">
                    <div className ="coin-value">
                        <div className='hide' style={animationStyle} className="coin-toss">
                            <p>{this.state.game.coinValue}</p>
                        </div>
                        </div>
                        <div className ="coin-value">
                        {this.state.game.starter === this.state.username && this.state.game.user1 &&
                           <p> your choice:{this.state.game.tossChoose.user1}</p>}
                        {this.state.game.recieved === this.state.username && this.state.game.user2 &&
                           <p> your choice:{this.state.game.tossChoose.user2}</p>}
                        {this.state.game.starter === this.state.username && this.state.game.user1 &&
                            <div>
                                {(this.state.game.tossChoose.user1 == this.state.game.coinValue) && <p>You win Toss</p>}
                                {((this.state.game.tossChoose.user1 !== this.state.game.coinValue)) && <p>You loss Toss</p>}
                            </div>}
                        {this.state.game.recieved === this.state.username && this.state.game.user2 &&
                            <div>
                                {(this.state.game.tossChoose.user2 == this.state.game.coinValue) && <p>You win Toss</p>}
                                {((this.state.game.tossChoose.user2 !== this.state.game.coinValue)) && <p>You loss Toss</p>}
                            </div>}
                        </div>
                     
                    </div>

                    <div className="single-header">
                     
                    </div>


                    </div>

                }


                {this.state.game.user1 &&
                    <div className="maingame">
                        <table className="table table-bordered" >
                            <tr>
                                <td onClick={() => { this.handlePosition(1) }}>{this.state.game.positions['1']}</td>
                                <td onClick={() => { this.handlePosition(2) }}>{this.state.game.positions['2']}</td>
                                <td onClick={() => { this.handlePosition(3) }}>{this.state.game.positions['3']}</td>
                            </tr>
                            <tr>
                                <td onClick={() => { this.handlePosition(4) }}>{this.state.game.positions['4']}</td>
                                <td onClick={() => { this.handlePosition(5) }}>{this.state.game.positions['5']}</td>
                                <td onClick={() => { this.handlePosition(6) }}>{this.state.game.positions['6']}</td>
                            </tr>
                            <tr>
                                <td onClick={() => { this.handlePosition(7) }}>{this.state.game.positions['7']}</td>
                                <td onClick={() => { this.handlePosition(8) }}>{this.state.game.positions['8']}</td>
                                <td onClick={() => { this.handlePosition(9) }}>{this.state.game.positions['9']}</td>
                            </tr>
                        </table>
                    </div>
                }
                 { this.state.game.win && this.state.game.win !== 'draw' && this.state.game.starter == this.state.username &&
                            <div>
                             { this.state.game.win == 'user1' && <p> game over win by You</p>}
                             { this.state.game.win == 'user2' && <p> You loss the game </p>}
                        </div>

                        }

                { this.state.game.win && this.state.game.win !== 'draw' &&this.state.game.recieved == this.state.username &&
                            <div>
                              { this.state.game.win == 'user2' && <p> game over win by You</p>}
                             { this.state.game.win == 'user1' && <p> You loss the game </p>}
                        </div>
                        }

                {this.state.game.win && this.state.game.win == 'draw' && <div><p> game over its draw</p></div>}
                {/* {this.state.game.win && <button onClick={this.playAgain}>play Again </button>} */}
            </div>
        )
    }
}

const mapstateToProps = (state) => {
    return {
        game: state.game.multiType,
        update: state.user.data
    }
}



export default connect(mapstateToProps, { updatePoints,updateGameData,GameMulti,GameInit })(Multiplayer)
