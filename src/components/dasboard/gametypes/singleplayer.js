import React, { Component } from 'react'
import { connect } from 'react-redux';
import { getDetails, empty } from '../../store/actions/authaction'
import { updatePoints } from '../../store/actions/usersaction'
import { BrowserRouter as Switch, Route, Redirect } from "react-router-dom";
class SinglePlayer extends Component {
    constructor(props) {
        super()
        this.state = {...props.gameType}
        this.handleCoinToss = this.handleCoinToss.bind(this)
        this.playAgain = this.playAgain.bind(this)
        // this.playAgain()
    }
    
    componentDidMount(){
        this.systemHandler()
    }

    handleTossChoose(data) {
        this.setState({
            tossChoose: data
        })
    }

    handleCoinToss(event) {
        var choose = Math.floor(Math.random() * (12 - 5)) + 5
        var b = 0
        this.setState({
            rotate: true
        })
        var st = setInterval(() => {
            if (choose == b) {
                clearInterval(st)
                this.setState({
                    rotate: false
                })
                setTimeout(() => {
                    if (this.state.tossChoose == this.state.coinValue) {
                        this.setState({
                            system: 'player2',
                            user: 'player1',
                            playingCoinType:'player1'
                        })
                    } else {
                        this.setState({
                            system: 'player1',
                            user: 'player2',
                            playingCoinType:'player1'
                        })
                        this.systemHandler()
                    }
                })

            }
            this.setState({
                coinValue: this.state.coinValue == 'Head' ? 'Tail' : 'Head'
            })
            b = b + 1;
        }, 500)

    }
    handlePosition(positionIndex){
        let positions = {...this.state.positions}
        if(this.state.playingCoinType === this.state.user){
          this.userHandler(positionIndex)
        }
    }

    getHowManyFree(){
        let postion = {...this.state.positions}
      return  Object.keys(postion).filter(ele => postion[ele])
    }

    setplayingCoinType(playingCoinType){
        this.setState({
            playingCoinType :playingCoinType
        })
    }

    winPosition(system,user){
   let win;
        (Object.keys(this.state.winPositions).map((ele)=>{
            let systemcount = [];
            let usercount = [];
           this.state.winPositions[ele].map((ele1)=>{
              if(this.state.positions[ele1] == system){
                 systemcount.push(ele1)
              }
              if(this.state.positions[ele1] == user){
                usercount.push(ele1)
             }
           })
           if(systemcount.length == 3){
              win = 'system'
           }else if(usercount.length == 3){
            win = 'user'
           } else {
               if(this.getHowManyFree().length == 9){
             win = 'draw'
               }
           }
        }))
        if(win){
            let points = 0
            this.setState({
                win : win
            })
            if(win === 'user'){
                points = 100;
            }else if(win === 'draw'){
                points = 50;
            }
            this.props.updatePoints({
                usernamw: localStorage.getItem('username'),
                points: points
            })
            this.props.UpdatePointsHandle({usernamw: localStorage.getItem('username'),points: points})
        }
    }

    withLocationChoose(system,user){
    let position 
    let systemtowin
    let usertowin
    let systempostion
     (Object.keys(this.state.winPositions).map((ele)=>{
         let systemcount = []
         let usercount = []
        this.state.winPositions[ele].map((ele1)=>{
           if(this.state.positions[ele1] == system){
              systemcount.push(ele1)
           }
           if(this.state.positions[ele1] == user){
            usercount.push(ele1)
         }
        })
        if(systemcount.length == 2){
            this.state.winPositions[ele].map((ele2)=> {
                if(systemcount.indexOf(ele2) == -1) {
                    if(!this.state.positions[ele2]){
                        systemtowin = ele2
                    }  
                }
            })
        }
        if(usercount.length == 2){
            this.state.winPositions[ele].map((ele2)=> {
                if(usercount.indexOf(ele2) == -1) {
                    if(!this.state.positions[ele2]) {
                        usertowin = ele2
                    }  
                    
                }
            })
        }
        if(systemcount.length == 1){
            this.state.winPositions[ele].map((ele2)=>{
                if(systemcount.indexOf(ele2) == -1){
                    if(!this.state.positions[ele2]){
                        systempostion = ele2
                    }  
                    
                }
            })
        }
     }))
     if(systemtowin){
      position = systemtowin
     } else if(usertowin){
      position = usertowin
     }else if(systempostion){
      position = systempostion
     }else{
        var choose = Math.floor(Math.random() * (8-2)) + 2
        if(!this.state.positions[choose]){
           position = choose
        }else{
            position = choose + 1 
        }
     }
    
     return position

    }

    systemHandler(){
     let freePosition = this.getHowManyFree()
     let state = {...this.state}
     if(freePosition.length >= 0){
        let system = state[state.system]
        let user = state[state.user]
       let positionIndex = this.withLocationChoose(system,user)
        state.positions[positionIndex]= this.state[this.state.system]
        this.setState({
            positions : state.positions
        })
        setTimeout(()=>{this.winPosition(system,user)},50)
        let playingCoinType =  this.state.playingCoinType == 'player1' ? 'player2': 'player1'
          this.setplayingCoinType(playingCoinType)
     }
    }
    
    userHandler(positionIndex) {
        let state = { ...this.state }
        let system = state[state.system]
        let user = state[state.user]
        if (!state.positions[positionIndex] && !this.state.win) {
            state.positions[positionIndex] = this.state[this.state.user]
            this.setState({
                positions: state.positions
            })
            let playingCoinType = this.state.playingCoinType == 'player1' ? 'player2' : 'player1'
            this.setplayingCoinType(playingCoinType)
            setTimeout(() => {
                this.winPosition(system, user)
                setTimeout(() => {
                    if (!this.state.win)
                        this.systemHandler()
                }, 50)
            }, 50)

        } else {
        // this.setState({
        //     error.me : state.positions
        // })
       }
    }
    playAgain(){
        this.props.gameType.positions = {1:'',2:'',3:'',4:'',5:'',6:'',7:'',8:'',9:''}
        Object.keys(this.props.gameType).map((ele)=>{
            this.setState({[ele]:this.props.gameType[ele]})
        })
    }

    render() {
        let animationStyle = {
            animation: this.state.rotate && 'spin .5s infinite linear'
        }

        return (
            <div>
                { !this.state.system &&
                    <div className="single-header">
                        <p>please to choose to win</p>
                        <div>
                        <input type="radio" checked={this.state.tossChoose === 'Head'} name="toss" value="Head" onChange={() => this.handleTossChoose('Head')} />Head
                          <input type="radio" name="toss" value="Tail" onChange={() => this.handleTossChoose('Tail')} />Tail
                          </div>
                        <button onClick={this.handleCoinToss}>TakeToss</button>
                    </div>

                }

                <div className="single-header">
                <div className ="coin-value">
                <div className='hide' style={animationStyle} className="coin-toss">
                        <p>{this.state.coinValue}</p>
                    </div>
                </div>
                    <div className ="coin-value">
                    {this.state.system && <p> your choice:{this.state.tossChoose}</p>}
                    {this.state.system == 'player1' && <p>You Loss Toss</p>}
                    {this.state.user == 'player1' && <p>You Win Toss</p>}
                    </div>
                  
                </div>
                <div className="single-header">
                {  this.state.system && 
                    <div className="maingame">
                        <table className="table table-bordered" >
                            <tr>
                                <td onClick={()=>{this.handlePosition(1)}}>{this.state.positions['1']}</td>
                                <td onClick={()=>{this.handlePosition(2)}}>{this.state.positions['2']}</td>
                                <td onClick={()=>{this.handlePosition(3)}}>{this.state.positions['3']}</td>
                            </tr>
                            <tr>
                                <td onClick={()=>{this.handlePosition(4)}}>{this.state.positions['4']}</td>
                                <td onClick={()=>{this.handlePosition(5)}}>{this.state.positions['5']}</td>
                                <td onClick={()=>{this.handlePosition(6)}}>{this.state.positions['6']}</td>
                            </tr>
                            <tr>
                                <td onClick={()=>{this.handlePosition(7)}}>{this.state.positions['7']}</td>
                                <td onClick={()=>{this.handlePosition(8)}}>{this.state.positions['8']}</td>
                                <td onClick={()=>{this.handlePosition(9)}}>{this.state.positions['9']}</td>
                            </tr>
                        </table>
                     { this.state.win && this.state.win !== 'draw' && <p> game over win buy {this.state.win}</p>}
                     { this.state.win && this.state.win == 'draw' && <p> game over its draw</p> } 
                     { this.state.win && <button onClick={this.playAgain}>play Again </button>}
                     </div>
                } 
                </div>
                

               
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        status: state.auth.status,
        data: state.auth.data.data,
        redirect: state.auth.redirect
    }
}



export default connect(mapStateToProps, { updatePoints })(SinglePlayer)
