import  React, { Component} from 'react'
import {connect} from'react-redux';
import {getDetails, empty} from './../store/actions/authaction'
import {GameInit} from './../store/actions/gameaction'
import {updateMessage,getConnectedUsers,getMessagesConnected} from './../store/actions/usersaction'
import { BrowserRouter as Switch, Route, Redirect } from "react-router-dom";
import Singleplayer from './gametypes/singleplayer';
import Multiplayer from './gametypes/multiplayer';
import { socket } from './../store/actions/socket';
import Gamelist from './../gamelist'
class Dasboard extends Component {
    constructor(props){
        super()
        this.state = {
            redirect: false,
            singleplayer:true,
            multiplayer:false,
            points: 0,
            username:localStorage.getItem('username'),
            multiplayerOn:false,
            singleType: {
                player1: 'X',
                player2: 'O',
                system: '',
                user: '',
                coinValue: "Head",
                tossChoose: "Head",
                rotate: false,
                tossWin: '',
                playingCoinType:'',
                count:{
                    system:0,
                    user:0
                },
                positions:{
                    1:'',
                    2:'',
                    3:'',
                    4:'',
                    5:'',
                    6:'',
                    7:'',
                    8:'',
                    9:''
                },
                winPositions:{
                    1:[1,2,3],
                    2:[4,5,6],
                    3:[7,8,9],
                    4:[1,4,7],
                    5:[2,5,8],
                    6:[3,6,9],
                    7:[1,5,9],
                    8:[3,5,7],
                },
                error:{
                 message:''
                },
                win:'',
                username: localStorage.getItem('username')
            },
            multiType:{
                username: localStorage.getItem('username'),
                alldata: {},
                socket: {
                    other: '',
                    socket: '',
                    to: '',
                    connect: false
                },
                game: {
                    player1: 'X',
                    player2: 'O',
                    user1: '',
                    user2: '',
                    coinValue: "Head",
                    tossChoose: {
                        user1: 'Head',
                        user2: 'Tail'
                    },
                    rotate: false,
                    tossWin: '',
                    playingCoinType: '',
                    count: {
                        user1: 0,
                        user2: 0
                    },
                    positions: {
                        1: '',
                        2: '',
                        3: '',
                        4: '',
                        5: '',
                        6: '',
                        7: '',
                        8: '',
                        9: ''
                    },
                    winPositions: {
                        1: [1, 2, 3],
                        2: [4, 5, 6],
                        3: [7, 8, 9],
                        4: [1, 4, 7],
                        5: [2, 5, 8],
                        6: [3, 6, 9],
                        7: [1, 5, 9],
                        8: [3, 5, 7],
                    },
                    error: {
                        message: ''
                    },
                    win: '',
                    username1: '',
                    username2: '',
                    socket: '',
                    starter: '',
                    recieved: '',
                    connect:false,
                    roomName:''
                },
                
            },
            chat:{
                text:'', 
                chatdata:[],
                roomName:'',
                tochat:'',
                typeing:false
            },
            connectedUsers: [],
            chatListVisible : false,
            gameListData:[],
            gameListVisible:false,
            messageread:{},
            messagePopup:''
        }
    this.chatSubmit = this.chatSubmit.bind(this)


    socket.on('chat-connection',(room)=>{
        console.log(">>>>>> chat-connection",room)
       socket.emit('join',room.roomName,this.state.username)
       this.setState({
           chat:{...this.state.chat,roomName:room.roomName}
       })
    })
    socket.on('messageread',(messageread)=>{
        console.log(">>>>>> chat-connection",messageread)
        this.setState({
            messageread:messageread
        })
    })
    socket.on('message-popup',(user)=>{
        console.log(">>>>>> message-popup",user)
        this.setState({
            messagePopup : user
        })
        setTimeout(() => {
            this.setState({
                messagePopup : ''
            }) 
        }, 5000);
    })
    socket.on('message',(message)=>{
        if(message.roomName === this.state.chat.roomName){
            this.props.messagesConnected.push(message)
            this.setState({
                chat: {...this.state.chat,chatdata : this.state.chat.chatdata}
            })
        } else {
            this.state.connectedUsers.map((users)=>{
                if(users.roomName === message.roomName && this.state.username != message.sender){
                   users.not_read += 1 
                }
             })
             this.setState({connectedUsers:this.state.connectedUsers})
        }
       
     })
     socket.on('show_iconnected',(room)=>{
        console.log(this.state.chat)
        this.state.connectedUsers.map((users)=>{
            if(users.roomName == room.room){
                users.active= room.active
            }
        })
        this.setState({connectedUsers: this.state.connectedUsers})
     })

     socket.on('show-room-disconnected',()=>{
        this.state.connectedUsers.map((users)=>{
           socket.emit('iconnected',users.roomName)
        })
        
     })

     socket.on('show_typeing',(room)=>{
         if(room === this.state.chat.roomName){
            this.setState({
                chat: {...this.state.chat,typeing:true}
            })
         }
     })

     socket.on('show_stopped',(room)=>{
        if(room === this.state.chat.roomName){
            this.setState({
                chat: {...this.state.chat,typeing:false}
            })
         }
    })

    this.handleKeyUp = this.handleKeyUp.bind(this)
    this.handleKeyPress = this.handleKeyPress.bind(this)
    this.handleCloseChat = this.handleCloseChat.bind(this)
    this.handleCloseChatList = this.handleCloseChatList.bind(this)
    this.handleCloseGameList = this.handleCloseGameList.bind(this)
    this.chatSection = React.createRef();
    }
    
    componentWillMount(){

     let username  = localStorage.getItem('username')
     if(username != this.props.match.params.username){
      this.setState({redirect:true})
     } else {
        setTimeout(() => {
            socket.emit('set-data',{
                username:localStorage.getItem('username'),
                socket:localStorage.getItem('socket')
            })
        }, 500);
       
         this.props.getDetails()
     }
    //  setTimeout(() => {
        this.props.getConnectedUsers()
    //  }, 1000);
           
      
    }
    componentDidUpdate(props){

        if(this.props.redirect){
            this.setState({redirect:true})
        }
       
    }

    componentWillReceiveProps(props){
        
        if(props.data){
            this.setState({points: props.data.points})
        }
        setTimeout(()=>{
            if(props.connectedUsers){
                let connectedUsers = props.connectedUsers.map((users)=>{
                    console.log(">>>>>???????/",this.state.messageread,this.state.multiplayer.username)
                  if(this.state.messageread&&this.state.messageread[users.roomName] && this.state.messageread[users.roomName][this.state.username]){
                    users.not_read = this.state.messageread[users.roomName][this.state.username]
                  }
                  console.log("meassage red..........",users)
                  return users
                })
                this.setState({connectedUsers: connectedUsers})
            }
        },0)
        
       
       
    }
    chatSubmit(){
        console.log('chat',this.state.chat)
        if(this.state.chat.text){
            let message =  {
                active:true,
                deleted:false,
                message:this.state.chat.text,
                roomName:this.state.chat.roomName,
                createdAt: new Date(),
                sender:this.state.multiType.username
            }
            this.props.messagesConnected.push(message)
            socket.emit('message',message)
            this.props.updateMessage(message)
            this.setState({
                chat: {...this.state.chat,chatdata : this.state.chat.chatdata,text:''}
            })
            setTimeout(() => {
                this.chatSection.current.scrollTop = this.chatSection.current.scrollHeight;
            }, 10);
        }
        
    }

    handleLogout() {
        window.localStorage['username']=''
        window.localStorage['token']=''
        window.localStorage['id']=''
        window.location.reload();
      }

      handlePlayerSelect(data){
          if(this.state.multiplayer && data == 'single'){
        //    if(this.state.multiplayerOn){
            socket.emit('end',{
                username:localStorage.getItem('username'),
                socket:localStorage.getItem('socket')
            })
            setTimeout(()=>{
                this.UpdatePoints({points:-100})
            },1000)
           
        //    }
          
          }
        this.setState({
            singleplayer: data === 'single'? true:false,
            multiplayer: data === 'multi'? true:false
        })
        this.props.GameInit()
      }

      UpdatePoints(data){
              this.props.getDetails()
            // this.setState({points: (this.state.points + data.points)})
      }
      handlemultiplayerOn(data){
          this.setState({multiplayerOn:data})
      }
      toOpenChatBox(room){
        socket.emit('join',room.roomName,this.state.username)
        let tochat =  room.users[0] === this.state.username?room.users[1]:room.users[0]
        
        this.props.getMessagesConnected(room.roomName).then((res)=>{
            // let chatdatas = this.props.messagesConnected
            // this.setState({...this.state.chat,chatdata: chatdatas})
            this.setState({
                chat:{...this.state.chat,tochat :tochat,roomName:room.roomName}
            })
          this.chatSection.current.scrollTop = this.chatSection.current.scrollHeight;
        })

        if(this.state.messageread[room.roomName] && this.state.messageread[room.roomName][this.state.username]){
             this.state.messageread[room.roomName][this.state.username] = 0
          }
          
        this.state.connectedUsers.map((users)=>{
            if(users.roomName === room.roomName){
               users.not_read  = 0 
            }
         })
         socket.emit('message-reset',{room:room.roomName,username:this.state.username})
        // let document  = ReactDOM.findDOMNode().getElementsByClassName('chatlist')
         this.setState({connectedUsers:this.state.connectedUsers,messageread:this.state.messageread})
      }

      updateChatRoomHandle(roomName){
        socket.emit('join',roomName,this.state.username)
          console.log('this.updateChatRoom',roomName,this.state.connectedUsers)
        this.setState({
            chat:{...this.state.chat,roomName:roomName}
        })
        let userdata =[]
        let newusers = roomName.split('_')
        userdata =  this.state.connectedUsers.filter((room)=> {
            if(room.roomName == roomName){
                return room
            } 
            if(room.users.indexOf(newusers[0])>=0 && room.users.indexOf(newusers[1])>=0){
                return room
            } 
        })
        console.log('userdata',userdata)
       if(userdata.length == 0){
        let users = roomName.split('_')
        this.state.connectedUsers.push({
            roomName,
            users,
            active :false,
           not_read : 0
        })
        this.toOpenChatBox({
            roomName,
            users,
            active :false,
           not_read : 0
        })
      this.setState({connectedUsers:this.state.connectedUsers})
       } else{
        this.toOpenChatBox(userdata[0])
       }
    
      }
      handleKeyPress(){
        socket.emit('typeing',this.state.chat.roomName)
      }

      handleKeyUp(){
          setTimeout(() => {
            socket.emit('stopped',this.state.chat.roomName)
          }, 3000);
    }
    handleCloseChat(){
        this.setState({
            chat:{...this.state.chat,roomName:''}
        })
    }
    handleCloseChatList(){
        this.setState({
            chatListVisible:!this.state.chatListVisible
        })
    }
    handleCloseGameList(){
        this.setState({
            gameListVisible:!this.state.gameListVisible
        })
    }
     

     render(){

    let loadstyle = {
        marginTop:'250px'
     }
     let active = {
        border: '3px solid red'
     }
     
    if(this.state.redirect){
        return (
            <div>
                 <Redirect to={{pathname: '/',state:{form: this.props.location}}}/>  
            </div>
        )
      }
      if(!this.props.data){
        return (
            <div style={loadstyle}>
               loading......
            </div>
        )
      }
      if(this.props.data){
        let chatData =[]
        let chatConnectedUsers =[]
        if(this.state.chat.chatdata && this.state.chat.chatdata.length){
          chatData.push(this.state.chat.chatdata.map((data)=>{
                return(
                    <li>{data.message}</li>
                )
               }))
          }
          if(this.props.messagesConnected && this.props.messagesConnected.length){
            chatData.push(this.props.messagesConnected.map((data)=>{
                  return(
                      <li>
                    <div  className={data.sender == this.state.username?'chatRight':'chatLeft'}>
                         {data.message} 
                    </div>
                      
                      </li>
                  )
                 }))
            }
          if(this.state.connectedUsers && this.state.connectedUsers.length){
            chatConnectedUsers.push(this.state.connectedUsers.map((data)=>{
            return(
                <li onClick={()=>this.toOpenChatBox(data)} >
                <span className="firstLetter">{data.users[0] == this.state.username?data.users[1][0]:data.users[0][0]}</span>    
            <span className="userName">{data.users[0] == this.state.username?data.users[1]:data.users[0]} {data.active &&<span className='chatactive'></span>}{data.not_read  !==0 &&<span className='chatnotread'>{data.not_read}</span>}</span></li>
            )
           }))
          }
        return (
            <div>
               <header>
                <nav>
                    <a className="logo" href="#"><img src="./images/logo.png" width="40px" height="40px"/></a>
                    <a href="#">Home</a>
                    <div className="navright">
                    <a href="#" onClick={()=>{this.handleLogout()}}>logout</a>
                    </div>
                    
                </nav>
                </header>
                <div className="dashboard"> 
                   <h2> {this.props.data.username}</h2>
                   <h2> points: {this.state.points}</h2>
                   <div className='gameType'>
                   <div className='gameTypeButton'>
                       <button style={this.state.singleplayer ? active: {}} onClick={()=>{this.handlePlayerSelect('single')}}>single</button>
                       <button style={this.state.multiplayer ? active: {}} onClick={()=>{this.handlePlayerSelect('multi')}}>multiple</button>
                   </div>
                   {this.state.singleplayer && <Singleplayer gameType = {this.props.game.single} UpdatePointsHandle = {this.UpdatePoints.bind(this)}/>}
                   {this.state.multiplayer && <Multiplayer multiplayerOn = {this.handlemultiplayerOn.bind(this)} updateChatRoom={this.updateChatRoomHandle.bind(this)} gameType = {this.props.game.multi} UpdatePointsHandle = {this.UpdatePoints.bind(this)}/>}  
                   </div>
                </div>
                { this.state.messagePopup &&
                     <div className="message-popup" >
                       you have request from {this.state.messagePopup}
                 </div>}
                { this.state.chat.roomName && <div className="chat">
                    <div className="chatHeading">
                    <p>{this.state.chat.tochat}</p>
                    <span onClick={this.handleCloseChat} className="icon">&#x2715;</span>
                    </div>
                    <div className="chatlist" ref={this.chatSection}>
                      
                            {chatData }
                   { this.state.chat.typeing&& <li><div className='chatLeft'>
                        ....
                    </div></li>}
                    </div>
                   <input type="text" onKeyPress={this.handleKeyPress} onKeyUp={this.handleKeyUp} onChange={(data) => this.setState({chat:{...this.state.chat,text:data.target.value}})} value={this.state.chat.text} /><button onClick={this.chatSubmit}>submit</button>
                 </div>}
                 { this.state.connectedUsers.length && this.state.chatListVisible &&
                     <div className="connectedChat">
                           <div className="chatHeading">
                    <p>Chat List</p>
                    <span onClick={this.handleCloseChatList} className="icon">&#x2715;</span>
                    </div>
                    <div className="connectedList">
                            {chatConnectedUsers }
                    </div>
                   
                 </div>}
                 { !this.state.chatListVisible &&
                     <div className="chatListButton" onClick={this.handleCloseChatList}>
                   chatList 
                 </div>}
                 { this.state.gameListVisible &&
                     <div className="gmalist">
                         <div className="chatHeading">
                             <p>GameList</p> <span onClick={this.handleCloseGameList} className="icon">&#x2715;</span> 
                             </div>
                         <div className="gameListData">
                            { <Gamelist/>}
                             </div>
                     </div>
                 }

                { !this.state.gameListVisible &&
                     <div className="gmalist" onClick={this.handleCloseGameList}>
                       gamesList 
                 </div>}
            </div>
        )
      }
    
}
}
const mapStateToProps = (state)=>{
    return  {
      status : state.auth.status,
      data:  state.auth.data.data,
      redirect : state.auth.redirect,
      game: state.game,
      connectedUsers: state.chat.getConnectedUsers,
      sendMessage : state.chat.updateMessage,
      messagesConnected: state.chat.getMessagesConnected,
    }
  }
  
  
  
  export default connect(mapStateToProps,{updateMessage,getMessagesConnected, getConnectedUsers,getDetails, empty,GameInit})(Dasboard)
  