import  React, { Component} from 'react'
import {connect} from'react-redux';
import {getDetails, empty} from './store/actions/authaction'
import {getGameData} from './store/actions/usersaction';
class Gamelist extends Component {
    constructor(props){
        super()
     this.state ={
         username : localStorage.getItem('username')
     }
    }
    componentWillMount(){
    this.props.getGameData()
    }
 
    componentWillReceiveProps(props){
       console.log(":::::::::::::::",props)
    }

     render(){
    let loadstyle = {
        marginTop:'250px'
     }
      if(!this.props.data || !this.props.gameData.length){
        return (
            <div style={loadstyle}>
               loading......
            </div>
        )
      }
      if(this.props.data && this.props.gameData.length){
          let gamelist = this.props.gameData.map((gamedata)=>{
            
              return (
              <li>
                   <div style={{'float':'left'}}>
                   <tr>
                   <td>{gamedata.username[0] === this.state.username ? 'me' : gamedata.username[0]  } </td> <td>{ gamedata.username[0] === gamedata.tossWinner ?<i class="fa fa-thumbs-up"> </i> : <i class="fa fa-thumbs-down"></i>}</td> <td>{ gamedata.username[0] === gamedata.gameWinner ?<i class="fa fa-thumbs-up"> </i> : <i class="fa fa-thumbs-down"></i>}</td>
                   </tr>
                   <tr>
                   <td>{gamedata.username[1] === this.state.username ? 'me' : gamedata.username[1]}</td> <td> { gamedata.username[1] === gamedata.tossWinner ?<i class="fa fa-thumbs-up"> </i> : <i class="fa fa-thumbs-down"></i>}</td> <td> { gamedata.username[1] === gamedata.gameWinner ?<i class="fa fa-thumbs-up"> </i> : <i class="fa fa-thumbs-down"></i>}</td>
                   </tr>
                   </div>
                   <div  style={{'float':'right',width:'50px'}}>
                       <table>
                           <tr>
                               <td>{gamedata.pattern[0][1]}</td>
                               <td>{gamedata.pattern[0][2]}</td>
                               <td>{gamedata.pattern[0][3]}</td>
                           </tr>
                           <tr>
                               <td>{gamedata.pattern[0][4]}</td>
                               <td>{gamedata.pattern[0][5]}</td>
                               <td>{gamedata.pattern[0][6]}</td>
                           </tr>
                           <tr>
                               <td>{gamedata.pattern[0][7]}</td>
                               <td>{gamedata.pattern[0][8]}</td>
                               <td>{gamedata.pattern[0][9]}</td>
                           </tr>
                       </table>
                   </div>
              </li>
          )
          })
        return (
            <span>  
            <li>
            <tr><td></td><td>toss</td><td>game</td><td>pattern</td></tr>
            </li>
            {gamelist}
            
            </span>
          
        )
      }
    
}
}
const mapStateToProps = (state)=>{
    return  {
        status : state.auth.status,
        data:  state.auth.data.data,
        redirect : state.auth.redirect,
        gameData: state.game.gameList
    }
  }
  
  
  
  export default connect(mapStateToProps,{ empty,getGameData})(Gamelist)
  