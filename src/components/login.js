import React , { Component}  from 'react';
import {connect} from'react-redux';
import {logIn,empty} from './store/actions/authaction'
import { BrowserRouter as Router, Redirect } from "react-router-dom";
class Login extends Component {

    constructor(props){
       super(props);
       this.state = {
           username : '',
           password:'',
           error :{
            username : '',
            password:'',
           }
       }
      this.handleChanges =  this.handleChanges.bind(this)
      this.submitChanges =  this.submitChanges.bind(this)
    }
    handleChanges = (event)=>{
     let name = event.target.name
     let value = event.target.value
     this.setState({
     [name] : event.target.value
     })
     let error = { ...this.state.error}
     error =  this.validation(name,value,error)
     this.setState({error :error})
    }
    submitChanges = (event)=>{
      event.preventDefault();
      let valid = true;
      let error = {...this.state.error}
      Object.keys(error).map((value)=>{
       error = this.validation(value.toString(),this.state[value],error)
        if(error[value] !== ''){
         valid = false
        }
      })
      this.setState({error})
      if(valid){
        let data = { ...this.state}
        delete data.error
         this.props.logIn(data).then((res)=>{
         })
       }
     }
    

  validation(name,value,error) {
    switch(name){
      case 'username':
          error.username = value  ? value.length > 3 ? "" : 'please enter data length more than 3': 'please enter data'
          break;
      case 'password':
           error.password = value ? value.length > 3 ? "" : 'please enter password length more than 3': 'please enter data'
           break;
      default:
           break;
    }
    return error
  }
  
    render(){
      let errorStyle={
        color:'red'
      }

      if(this.props.redirect){
        this.props.empty()
        return (
        
             <Redirect to={{pathname: '/'+this.state.username,state:{form: this.props.location}}}/>
        )
      }
        return(
            <div className="signup">
              <h2>LogIn Here</h2>
                <form onSubmit={this.submitChanges}>
                {!this.props.status && <p style={errorStyle}>Please enter valid username and password</p>}
                    <div className="form-group">
                    <label htmlFor="inputEmail">UserName</label>
                      <input type="text" name="username" onChange ={this.handleChanges} className ="form-control"  value={this.state.username}/>
                      {this.state.error.username && <p style={errorStyle}>{this.state.error.username}</p>}
                    </div>
                    <div className="form-group">
                     <label htmlFor="">Password</label>
                      <input type="password" name="password" onChange ={this.handleChanges} className ="form-control"  value={this.state.password}/>
                      {this.state.error.password && <p style={errorStyle}>{this.state.error.password}</p>}
                    </div>
                    <div className="form-group">
                      <button type="submit" className ="form-control" > submit </button>
                    </div>
                    </form>
            </div>
        );
    }
}
const mapStateToProps = (state)=>{
  return  {
    status : state.auth.status,
    data:  state.auth.data,
    redirect : state.auth.redirect
  }
}



export default connect(mapStateToProps,{ logIn,empty })(Login)
