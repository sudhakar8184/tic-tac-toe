export default  {
    url: process.env.NODE_ENV != 'production'? 'http://localhost:7000/api/v1/': 'https://tictactoepoints.herokuapp.com/api/v1/',
    socket:process.env.NODE_ENV != 'production'? 'http://localhost:7000': 'https://tictactoepoints.herokuapp.com',
}